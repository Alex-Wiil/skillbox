# Задача 5. Песни
# Что нужно сделать
#
# Мы пишем приложение для удобного прослушивания музыки.
# У Вани есть список из девяти песен группы Depeche Mode.
# Каждая песня состоит из названия и продолжительности с точностью до долей минут:
#
# violator_songs = [
#     ['World in My Eyes', 4,86],
#     ['Sweetest Perfection', 4,43],
#     ['Personal Jesus', 4,56],
#     ['Halo', 4,9],
#     ['Waiting for the Night', 6,07],
#     ['Enjoy the Silence', 4,20],
#     ['Policy of Truth', 4,76],
#     ['Blue Dress', 4,29],
#     ['Clean', 5,83]]

# Из этого списка Ваня хочет выбрать N песен и закинуть их в особый плейлист
# с другими треками. И при этом ему важно, сколько времени в сумме эти N песен
# будут звучать.
#
# Напишите программу, которая запрашивает у пользователя количество песен из списка
# и затем названия этих песен, а на экран выводит общее время их звучания.

# Пример:
#
# Сколько песен выбрать? 3

# Название 1-й песни: Halo
# Название 2-й песни: Enjoy the Silence
# Название 3-й песни: Clean

# Общее время звучания песен: 14,93 минуты

violator_songs = \
    [['World in My Eyes', 4.86],['Sweetest Perfection', 4.43],
    ['Personal Jesus', 4.56],['Halo', 4.9],['Waiting for the Night', 6.07],
    ['Enjoy the Silence', 4.20],['Policy of Truth', 4.76],
    ['Blue Dress', 4.29],['Clean', 5.3]]


def testing_presence(my_list, song):
    for k in my_list:
        if k == song:
            return True
    return False
def find_songs_duration(song, name):
    duration = 0
    for k in range(len(song)):
       if song[k] == name:
           duration += song[k + 1]
    return duration

times_sum = 0
songs_num = int(input('Сколько песен выбрать? '))
flag = True
for song in range(songs_num):

    print('Название ' + str(song + 1) + '-й песни: ', end = '')
    song_name = input('')
    mistake = False
    for song in violator_songs:
        if testing_presence(song, song_name) == True:
            times_sum += find_songs_duration(song, song_name)
            result = 'Общее время звучания песен:', times_sum

print(*result)
