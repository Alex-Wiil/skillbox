# Задача 7. Ролики
# Что нужно сделать
#
# Частная контора даёт в прокат ролики самых разных размеров.
# Человек может надеть ролики любого размера, которые не меньше размера его ноги.
# Пользователь вводит два списка размеров: N размеров коньков и K размеров ног людей.
# Реализуйте код, который определяет, какое наибольшее число человек сможет одновременно взять
# ролики и пойти покататься.
#
# Пример:
# Кол-во коньков: 4
# Размер 1-й пары: 41
# Размер 2-й пары: 40
# Размер 3-й пары: 39
# Размер 4-й пары: 42

# Кол-во людей: 3
# Размер ноги 1-го человека: 42
# Размер ноги 2-го человека: 41
# Размер ноги 3-го человека: 42
#
# Наибольшее кол-во людей, которые могут взять ролики: 2
def skates_distribution(skates, peoples):
    counter = 0
    for i in skates:
        if i in peoples:
            peoples.remove(i)
            counter += 1
    return counter

peoples_num = int(input('\nКол-во коньков: '))
peoples_ls = []
for i in range(peoples_num):
    print('Размер', i + 1, end = '')
    pairs_size = int(input('-й пары: '))
    peoples_ls.append(pairs_size)

skates_num = int(input('\nКол-во людей: '))
skates_ls = []
for i in range(skates_num):
    print('Размер ноги', i + 1, end = '')
    peoples_size = int(input('-го человека: '))
    skates_ls.append(peoples_size)

print('\nНаибольшее кол-во людей, которые могут взять ролики:', \
    skates_distribution(skates_ls, peoples_ls))