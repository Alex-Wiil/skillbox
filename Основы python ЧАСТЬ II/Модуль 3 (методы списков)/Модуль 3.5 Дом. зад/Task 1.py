# Задача 1. Страшный код
# Что нужно сделать
# Вашему другу, который тоже начал изучать Python, преподаватель дал такую задачу:
# есть три списка — основной и два побочных. В основном лежат элементы [1, 5, 3],
# а в побочных — [1, 5, 1, 5] и [1, 3, 1, 5, 3, 3] соответственно.
# Первый побочный закидывается в основной, там считается количество цифр 5,
# количество выводится на экран, и затем они удаляются из основного списка.
# После этого в основной закидывается второй побочный список, там считается количество цифр 3 и
# выводится на экран. В конце также выводится и сам список.
# Из интереса вы попросили вашего друга показать код его программы и поняли,
# что сделали это не зря — то, что вы увидели, повергло вас в шок и ужас.

#    Вот сам код:
# a = [1, 5, 3]
# b = [1, 5, 1, 5]
#c = [1, 3, 1, 5, 3, 3]
#for i in b:
 #   a.append(i)
#t = 0
#for i in a:
#    if i == 5:
#        t += 1
# print(t)
# d = []
# for i in a:
 #   if i != 5:
 #       d.append(i)
#for i in c:
#     d.append(i)
# t = 0
# for i in d:
#     if i == 3:
#        t += 1
# print(t)
# print(d)
# Используя знания о методах списков, а также о стиле программирования,
# помогите другу переписать программу.
# Не используйте дополнительные списки.
# Результат работы программы:
# Кол-во цифр 5 при первом объединении: 3
# Кол-во цифр 3 при втором объединении: 4
# Итоговый список: [1, 3, 1, 1, 1, 3, 1, 5, 3, 3]
#
#       ПЕРВЫЙ СПОСОБ
main_list = [1, 5, 3]
fir_list = [1, 5, 1, 5]
sec_list = [1, 3, 1, 5, 3, 3]
for i in fir_list:
    main_list.append(i)
counter = 0
for num in main_list:
    if num == 5:
        counter += 1
print('Кол-во цифр 5 при первом объединении:', counter)
nw_main_list = []
for num in main_list:
    if num != 5:
        nw_main_list.append(num)
for num in sec_list:
    nw_main_list.append(num)
counter = 0
for num in nw_main_list:
    if num == 3:
        counter += 1

main_list = []

for num in nw_main_list:
    if num != 3:
        main_list.append(num)
print('Кол-во цифр 3 при втором объединении:', counter)
print('Итоговый список:', main_list)

#       ВТОРОЙ СПОСОБ
main_list = [1, 5, 3]
fir_list = [1, 5, 1, 5]
sec_list = [1, 3, 1, 5, 3, 3]
nw_main_list = []

main_list.extend(fir_list)
print('\nКол-во цифр 5 при первом объединении:', main_list.count(5))
for num in main_list:
    if num != 5:
        nw_main_list.append(num)

nw_main_list.extend(sec_list)
print('Кол-во цифр 3 при втором объединении:', nw_main_list.count(3))
main_list = []

for num in nw_main_list:
    if num != 3:
        main_list.append(num)
print('Итоговый список:', main_list)