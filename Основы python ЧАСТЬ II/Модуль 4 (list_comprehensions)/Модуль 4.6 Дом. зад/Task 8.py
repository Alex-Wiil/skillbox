# Задача 8. Развлечение
# Что нужно сделать
# N палочек выставили в один ряд, пронумеровав их слева направо числами от 1 до N.
# Затем по этому ряду бросили K камней, при этом i-й камень сбил все палки с номерами от L_i до R_i включительно.
# Определите, какие палки остались стоять на месте.
# Напишите программу, которая получает на вход количество палок N и количество бросков K.
# Далее идёт K пар чисел Left_i, Right_i, при этом 1 ≤ Left_i ≤ Right_i ≤ N.
# Программа должна вывести последовательность из N символов, где j-й символ есть “I”,
# если j-я палка осталась стоять, или “.”, если j-я палка была сбита.
#
# Пример:
# Количество палок: 10
# Количество бросков: 3
# Бросок 1. Сбиты палки с номера 8
# по номер 10.
# Бросок 2. Сбиты палки с номера 2
# по номер 5.
# Бросок 3. Сбиты палки с номера 3
# по номер 6.
#
# Результат: I.....I...

from random import*
sticks_num = 10#int(input('Количество палок: '))
stics_lst = list(range(sticks_num))               # список состоящий из индексов палок
throws_num = 3#int(input('Количество бросков: '))
copy = stics_lst[:]           # копия осн. списка(в нем нахожу индексы сбитых палок)
def painting_stics(stics_lst, sticks_num): # функция которая выводит рисунок
        paint = ['.' for i in range(10)]   # изначальный рисунок
        for i in stics_lst:
            ls = ['I']
            paint[i], ls[0] = ls[0], paint[i]
        return paint

for i in range(throws_num):
    final_lst = []
    downed_stcks_num = randint(1, len(stics_lst) - 1)
    Left_i = downed_stcks_num + randint(-1, 0)
    Right_i = downed_stcks_num + randint(0, 1)
    print('\nБросок', str(i + 1) + '.', 'Сбиты палки с номера', copy.index(stics_lst[Left_i]) + 1)
    print('по номер', str(copy.index(stics_lst[Right_i]) + 1) + '.')
    for i in range(len(stics_lst)):
        if Left_i > i or i > Right_i:
            final_lst.append(stics_lst[i])
    stics_lst = final_lst

print('\nРезультат: ', end = '')
_ = [print(i, end = '') for i in painting_stics(stics_lst, sticks_num)]
