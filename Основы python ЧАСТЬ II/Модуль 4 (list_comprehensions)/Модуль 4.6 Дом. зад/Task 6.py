# Задача 6. Сжатие списка
# Что нужно сделать
# Дан список из N целых случайных чисел (число от 0 до 2).
# Напишите программу, которая выполняет «сжатие списка» — переставляет все нулевые элементы в конец массива.
# При этом все ненулевые элементы располагаются в начале массива в том же порядке.
# Затем все нули из списка удаляются.
#
# Пример:
# Количество чисел в списке: 10
# Список до сжатия: [0, 2, 1, 0, 0, 0, 1, 0, 2, 0]
# Список после сжатия: [2, 1, 1, 2]

from random import*

digs_num = int(input('Количество чисел в списке: '))
nums_lst = [randint(0, 2) for num in range(digs_num)]
sorted_list = [num for num in nums_lst if num != 0]
print('Список до сжатия:', nums_lst)
print('Список после сжатия:', sorted_list)
